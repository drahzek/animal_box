package com.drahzek;

import java.util.Scanner;
import com.drahzek.models.Animal;
/**
 * Main class of an application
 */
public class Main {
    public static void main(String[] args) {
        //Scanner feature for reading input
        Scanner inputScanner = new Scanner(System.in);

        Animal[] animals = new Animal[30];

        //loop running state
        boolean running = true;

        System.out.println("Animal box");

        while (running) {
            int command = inputScanner.nextInt();
            inputScanner.skip("\n");

            switch (command) {
                case 1: {
                    //local variables
                    System.out.println("Give your animal a name: ");
                    String name = inputScanner.nextLine();

                    System.out.println("Set up their age: ");
                    int age = inputScanner.nextInt();

                    //adding new animal
                    Animal animal = new Animal(name, age);

                    System.out.println("Give your animal an index: ");
                    int index = inputScanner.nextInt();
                    animals[index] = animal;
                    break;
                }
                case 2: {
                    System.out.println("What animal index do you want to see? ");
                    //reading existing animal index
                    int index = inputScanner.nextInt();
                    //printing info
                    System.out.println(animals[index].getName() + " " + animals[index].getAge());
                    break;
                }
                case 3: {
                    for (Animal animal : animals) {
                        if (animal != null) {
                            System.out.println(animal.getName() +" " + animal.getAge());
                        }
                    }
                }
                case 0: {
                    //ending the loop
                    running = false;
                    break;
                }
                default: {
                    System.out.println("Working commands:\n" +
                            "1 - add new animal \n" +
                            "2 - read existing animal \n" +
                            "0 - kill the app \n");
                    break;
                }
            }
        }
    }
}
