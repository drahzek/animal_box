package com.drahzek.models;

/**
 * class for the unspecified animals
 */
public class Animal {

    //animal characteristics
    private String name;
    private int age;

    //default constructor for the sake of convenience
    public Animal() {
    }

    //normal constructor
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
